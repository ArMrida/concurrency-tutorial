package concurrency;

import java.text.DecimalFormat;

public class CubePrinterRunnable implements Runnable {
	private final int number;

	public CubePrinterRunnable(int number) {
		this.number = number;
	}

	@Override
	public void run() {
		for (double i = 1; i < number; i++) {
			System.out.println(new DecimalFormat("#").format(i)+ " - "+new DecimalFormat("#").format(Math.pow(i, 3)));
			
		}
	}

}
