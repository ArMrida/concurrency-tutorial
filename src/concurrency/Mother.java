package concurrency;

public class Mother implements Runnable {

    private final Wallet wallet;

    public Mother(Wallet wallet) {
        this.wallet = wallet;
    }

    @Override
    public void run() {
        int counter = 0;
        while(counter < 10) {
            System.out.println("Mother takes 10 dollars, all money "
                + wallet.addMoney(10) + " dollar");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            counter++;
        }
    }
}
