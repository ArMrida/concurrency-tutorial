package concurrency;

public class Father implements Runnable {

    private final Wallet wallet;

    public Father(Wallet wallet) {
        this.wallet = wallet;
    }

    @Override
    public void run() {
        int counter = 0;
        while(counter < 10) {
            System.out.println("Father puts 20 dollars, all money "
                + wallet.addMoney(20) + " dollar");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            counter++;
        }
    }
}
