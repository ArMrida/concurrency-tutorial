package concurrency;

import java.util.List;

public class EvenNumbersRunnable implements Runnable {

    private final int max;
    private final List<Integer> numbers;

    public EvenNumbersRunnable(int max, List<Integer> numbers) {
        this.max = max;
        this.numbers = numbers;
    }

    @Override
    public void run() {
        for (int i = 2; i <= max; i += 2) {
            numbers.add(i);
        }
    }
}
