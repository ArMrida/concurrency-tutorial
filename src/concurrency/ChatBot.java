package concurrency;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public final class ChatBot {

    private final String name;
    private final String webhookUrl;
    private final Date startDate;
    private final List<String> commands;

    public ChatBot(String name, String webhookUrl, Date startDate, List<String> commands) {
        this.name = name;
        this.webhookUrl = webhookUrl;
        this.startDate = new Date(startDate.getTime());
        this.commands = Collections.unmodifiableList(new ArrayList<>(commands));
    }

    public String getName() {
        return name;
    }

    public String getWebhookUrl() {
        return webhookUrl;
    }

    public Date getStartDate() {
        return new Date(startDate.getTime());
    }

    public List<String> getCommands() {
        return commands;
    }

    public ChatBot withName(String name) {
        return new ChatBot(name, webhookUrl, startDate, commands);
    }

    public ChatBot withWebhookUrl(String webhookUrl) {
        return new ChatBot(name, webhookUrl, startDate, commands);
    }

    public ChatBot withStartDate(Date startDate) {
        return new ChatBot(name, webhookUrl, startDate, commands);
    }

    public ChatBot withCommands(List<String> commands) {
        return new ChatBot(name, webhookUrl, startDate, commands);
    }
}
