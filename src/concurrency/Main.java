package concurrency;

import java.util.Arrays;
import java.util.Date;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        ChatBot selina = new ChatBot(
                "Selina",
                "nope://selina/message",
                new Date(),
                Arrays.asList("help", "reddit_top", "medium_top"));

        ChatBot bob = selina.withName("Bob")
                .withWebhookUrl("nope://bob/message")
                .withStartDate(new Date());
    }
}
