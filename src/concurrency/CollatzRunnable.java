package concurrency;

import java.util.ArrayList;
import java.util.List;

public class CollatzRunnable implements Runnable {

    private final int number;

    public CollatzRunnable(int number) {
        this.number = number;
    }

    @Override
    public void run() {
        validateNumber(number);
        List<Integer> sequence = new ArrayList<>();
        int n = number;
        while (n != 1) {
            sequence.add(n);
            n = calcNextNumber(n);
        }
        sequence.add(n);

        System.out.println("Collatz sequence of " + number);
        System.out.println(sequence);
    }

    private void validateNumber(int number) {
        if (number < 1) {
            throw new IllegalArgumentException("Number must be greater than 1");
        }
    }

    private int calcNextNumber(int number) {
        if (number % 2 == 0) {
            return number / 2;
        } else {
            return 3 * number + 1;
        }
    }

}
