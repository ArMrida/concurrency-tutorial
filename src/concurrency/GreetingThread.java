package concurrency;

public class GreetingThread extends Thread {

    private static final int DEFAULT_MAX_GREETING = 10;

    private final int maxGreetingCount;

    public GreetingThread() {
        maxGreetingCount = DEFAULT_MAX_GREETING;
    }

    public GreetingThread(String name, int maxGreetingCount) {
        super(name);
        this.maxGreetingCount = maxGreetingCount;
    }

    
    
    
    @Override
    public void run() {
        int count = 0;
        while (count < maxGreetingCount) {
            System.out.println("Hello from " + Thread.currentThread().getName());
            count++;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
