package concurrency;

import java.util.concurrent.atomic.AtomicBoolean;

public class StoppableThread extends Thread {

    private final AtomicBoolean flag;

    public StoppableThread() {
        flag = new AtomicBoolean(true);
    }

    @Override
    public void run() {
        while(flag.get()) {
            System.out.println("Hi. I'm a stoppable thread." +
            "\nHit my stopThread method to stop me!");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void stopThread() {
        flag.set(false);
    }
}
