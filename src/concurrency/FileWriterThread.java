package concurrency;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FileWriterThread extends Thread {
	private final String text;
	private final String filename;
	public FileWriterThread(String text, String filename) {
		this.text=text;
		this.filename=filename;
	}
	
	public void writing(String text) {
        BufferedWriter output;
        try {
            output = new BufferedWriter(new FileWriter(filename, true));
            output.write(text);
            output.close();
        } catch (IOException ex) {
            System.err.println("There is some problem with file writing. " + ex.toString());
        }
    }
	
	@Override
	public void run() {
		writing(text);
	}
	
}
