package concurrency;

import java.util.concurrent.atomic.AtomicLong;

public class Wallet {

    private final AtomicLong money;

    public Wallet() {
        money = new AtomicLong(0);
    }

    public long addMoney(int amount) {
        return money.addAndGet(amount);
    }

    public long getMoney(int amount) {
        return money.addAndGet(-amount);
    }

}
